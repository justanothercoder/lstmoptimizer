import numpy as np

import theano
import theano.tensor as T
import lasagne as L

import agentnet
from agentnet import Recurrence
from agentnet.memory import LSTMCell
from collections import OrderedDict

from func_layer import FuncLayer
from grad_layer import GradLayer
from preprocess_layer import PreprocessLayer
from opt_step import OptStep
from theta_step import ThetaStep
from index_layer import IndexLayer
from lstm_step import LSTMStep

Inp = L.layers.InputLayer
Reshape = L.layers.ReshapeLayer
        
def make_lstm_net(function, params, input_var=None, n_steps=None, **kwargs):

    input_var = input_var or T.matrix('input_var_inside')

    l_input  = Inp(shape=(None, None), input_var=input_var)
    l_params = [Inp(shape=(None,) * param.ndim, input_var=param) for param in params]
    l_params_outer = [Inp(shape=(None,) * param.ndim, input_var=param) for param in params]

    l_grad = GradLayer(l_input, function, l_params)

    n_func, n_coords = input_var.shape

    l_lstm = PreprocessLayer(Reshape(l_grad, (n_func * n_coords,)), 
                             preprocess_input=kwargs.get('preprocess_input', True), p=kwargs.get('p', 10.), 
                             use_function_values=kwargs.get('use_function_values', False))

    recurrent_connections = OrderedDict({ })

    num_units = kwargs.get('num_units', 20)
    grad_clipping = kwargs.get('grad_clipping', 0)
    n_gac = kwargs.get('n_gac', 0)

    for i in range(kwargs.get('n_layers', 2)):
        l_lstm_cell = Inp(shape=(None, num_units))
        l_lstm_hid  = Inp(shape=(None, num_units))
        l_lstm = LSTMStep(l_lstm, l_lstm_cell, l_lstm_hid, num_units=num_units, n_gac=n_gac, grad_clipping=grad_clipping, name='lstm_{}'.format(i))

        l_cell = IndexLayer(l_lstm, 0)
        l_hid  = IndexLayer(l_lstm, 1)

        l_lstm = l_hid

        recurrent_connections.update({l_cell: l_lstm_cell, l_hid: l_lstm_hid})

    l_opt = OptStep(l_lstm, num_units, loglr=kwargs.get('loglr', True))
    l_opt = Reshape(l_opt, (n_func, n_coords))
    l_opt = ThetaStep(l_input, l_opt, scale_output=kwargs.get('scale_output', 1.0))
    recurrent_connections[l_opt] = l_input

    l_func = FuncLayer(l_opt, function, l_params)

    l_input_outer = Inp(shape=(None, None), input_var=input_var)

    n_steps = n_steps or T.iscalar('n_steps_inside')
    l_rec = Recurrence(
        state_variables=recurrent_connections,
        n_steps=n_steps,
        tracked_outputs=[l_opt, l_func],
        batch_size=(input_var.shape[0] * input_var.shape[1]),
        none_outs=(l_func,),
        gradient_steps=kwargs.get('gradient_steps', -1),
        unroll_scan=False,
        state_init={l_opt: l_input_outer},
        input_nonsequences={l_param: l_param_outer for l_param, l_param_outer in zip(l_params, l_params_outer)}
    )

    l_func_1 = FuncLayer(l_input_outer, function, l_params_outer)
    l_func_1 = L.layers.DimshuffleLayer(l_func_1, (0, 'x'))

    l_theta_1 = L.layers.DimshuffleLayer(l_input_outer, (0, 'x', 1))

    l_theta = L.layers.ConcatLayer([l_theta_1, l_rec[l_opt]], axis=1)
    l_loss  = L.layers.ConcatLayer([l_func_1, l_rec[l_func]], axis=1)

    return {
        'thetas': l_theta,
        'losses': l_loss,

        'l_rec': l_rec,
        'l_opt': l_opt,

        'theta': input_var,
        'n_steps': n_steps,

        'params': l_params_outer,
        'params_': l_params,
    }
