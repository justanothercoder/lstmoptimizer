import numpy as np
import sklearn.preprocessing

import theano
import theano.tensor as T
import lasagne as L


def elem_dot(A, B):
    return (A[..., None] * B[..., None, :, :]).sum(axis=-2)


def logistic_function(theta, X, y, lambd=1e-6):
    pred = T.clip(L.nonlinearities.sigmoid(X.dot(theta[1:]) + theta[0]), 1e-6, 1. - 1e-6)
    return L.objectives.binary_crossentropy(pred, y).mean() + lambd * (theta**2).sum()


def logistic_function_tensor(theta, X, y, lambd=1e-6):
    pred = T.clip(L.nonlinearities.sigmoid(elem_dot(X, theta[:, 1:].dimshuffle(0, 1, 'x')).dimshuffle(0, 1) + theta[:, 0].dimshuffle(0, 'x')), 1e-6, 1. - 1e-6)
    return L.objectives.binary_crossentropy(pred, y).mean(axis=1) + lambd * (theta**2).sum(axis=1)

    
def sample_point_and_params(n, ndim):
    X = np.random.normal(size=(n, ndim))
    y = np.random.randint(0, 2, size=n)

    theta = np.random.normal(size=ndim+1)

    return theta, (X, y)


def multisampler(n_func, n, ndim, *args):
    X = np.random.normal(size=(n_func, n, ndim))
    y = np.random.randint(0, 2, size=(n_func, n))
    theta = np.random.normal(size=(n_func, ndim+1))
    return theta, X, y


def zero_min_sampler(n_func, n, ndim, *args):
    X = np.random.normal(size=(n_func, n, ndim))
    w = np.random.normal(size=(n_func, ndim+1))

    y = (elem_dot(X, w[:, 1:, None])[:, :, 0] + w[:, :1] >= 0).reshape(n_func, n)
    theta = np.random.normal(size=(n_func, ndim+1))

    return theta, X, y

def normalized_sampler(batch_size, n, ndim): 
    scaler = sklearn.preprocessing.StandardScaler()
    temp = zero_min_sampler(batch_size, n, ndim)

    for i in range(batch_size):
        temp[1][i] = scaler.fit_transform(temp[1][i])

    return temp
