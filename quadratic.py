import numpy as np
import theano
import theano.tensor as T

def elem_dot(A, B):
    return (A[..., None] * B[..., None, :, :]).sum(axis=-2)


def quadratic_function(theta, W, b):
    return ((W.dot(theta) + b)**2).sum() / theta.shape[0]**2


def quadratic_function_tensor(theta, W, b):
    return ((elem_dot(W, theta.dimshuffle(0, 1, 'x')).dimshuffle(0, 1) + b)**2).sum(axis=1) / theta.shape[1]**2


def sample_point(ndim, *args):
    return np.random.normal(size=ndim)


def sample_params(ndim, *args):
    W = np.random.normal(size=(ndim, ndim))
    b = np.random.normal(size=ndim)
    return W, b
    

def sample_point_and_params(ndim=None, sample_point=sample_point):
    params = sample_params(ndim)
    theta = sample_point(ndim, *params)
    
    return theta, params
    

def random_direction(ndim):
    d = np.random.normal(size=ndim)
    d /= np.sqrt((d**2).sum())
    return d


def random_direction(ndim, num=1):
    d = np.random.normal(size=(num, ndim))
    d /= np.sqrt((d**2).sum(axis=1, keepdims=True))
    return d


def grid_sampler(n_functions, d, num=100, min_dim=2, max_dim=10):
    problems = {}
    for _ in range(n_functions):
        ndim = np.random.randint(min_dim, max_dim)
        W, b = sample_params(ndim)
        solution = np.linalg.pinv(W).dot(-b)

        random_dir = random_direction(ndim, num)
        random_dir /= np.sqrt((random_dir.dot(W.T)**2).sum(axis=1, keepdims=True))
            
        points = solution.reshape(1, -1) + d * random_dir
        Ws = np.tile(W, (num, 1, 1))
        bs = np.tile(b, (num, 1))
        
        if problems.get(ndim) is None:
            problems[ndim] = (points, Ws, bs)
        else:
            old_p, old_W, old_b = problems[ndim]
            problems[ndim] = (np.vstack([old_p, points]), np.vstack([old_W, Ws]), np.vstack([old_b, bs]))
        
    return problems


def multisampler(n, ndim, *args):
    W = np.random.normal(size=(n, ndim, ndim))
    b = np.random.normal(size=(n, ndim))
    theta = np.random.normal(size=(n, ndim))
    return theta, W, b
