import numpy as np
from collections import OrderedDict

class Optimizer:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)


    def optimize(self, n_iter, theta, params, **kwargs):
        raise NotImplementedError


    def optimize_many(self, n_iter, thetas, params, **kwargs):
        raise NotImplementedError


    def optimize_sym(self, n_iter, thetas, params, **kwargs):
        raise NotImplementedError


    def optimize_sym_grad(self, theta, grad, **kwargs):
        raise NotImplementedError


    def optimizer_loss(self, loss_history, loss_type='sum', M=np):
        if loss_type == 'sum':
            loss = loss_history.sum()
        elif loss_type == 'prod':
            loss = M.log(loss_history).sum()
        elif loss_type == 'weighted_prod':
            loss = (M.log(loss_history) * 0.9 ** M.arange(loss_history.shape[0])[::-1]).sum()
        elif loss_type == 'norm_sum':
            loss = (loss_history / loss_history[:, 0].reshape((loss_history.shape[0], 1))).sum()
        elif loss_type == 'rel_sum':
            loss = (loss_history[:, 1:] / loss_history[:, :-1]).sum()
        else:
            raise ValueError("Unknown loss type: {}".format(loss_type))

        return loss


    def get_updates(self, loss, params, **kwargs):
        grads = theano.grad(loss, params)

        theta = T.concatenate([T.flatten(p) for p in params])
        grad = T.concatenate([T.flatten(p) for p in grads])

        delta, opt_updates = self.optimize_sym_grad(theta, grad, **kwargs)

        updates = OrderedDict()

        cur_pos = T.constant(0)

        for p in params:
            next_pos = cur + T.prod(p.shape)
            d = delta[cur_pos : next_pos] 

            updates[p] = p + delta
            cur_pos = next_pos

        updates.update(opt_updates)
        return updates
