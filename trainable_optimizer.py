import time
import numpy as np

import theano
import theano.tensor as T
import lasagne as L

from optimizer import Optimizer

from util import iterate_minibatches

from collections import deque as Deque
        
class TrainableOptimizer(Optimizer):
    def __init__(self, **kwargs):
        super(TrainableOptimizer, self).__init__(**kwargs)

    
    def train_on_batch(self, batch, n_iter, **kwargs):
        theta = batch[0]
        params = batch[1:]

        thetas, losses = self.train_fn(theta, n_iter, *params)
        median_final_loss = np.median(losses, axis=0)[-1]
        train_loss = self.optimizer_loss(losses, self.loss_type) / theta.shape[0]

        return train_loss, median_final_loss, thetas


    def validation_loss(self, val_inputs, n_iter, batch_size, **kwargs):
        val_loss = 0
        n_batches = 0

        for inputs in iterate_minibatches(*val_inputs, batch_size=batch_size):
            _, loss_history = self.loss_fn(inputs[0], n_iter, *inputs[1:])

            val_loss += self.optimizer_loss(loss_history, self.loss_type) / batch_size
            n_batches += 1

        val_loss /= n_batches
        return val_loss


    def train(self, train_inputs, val_inputs=None, batch_size=100, n_iter=100, **kwargs):
        train_loss = []
        val_loss = []
        problems = []

        n_epochs   = kwargs.get('n_epochs', 50)
        decay_rate = kwargs.get('decay_rate', 0.96)
        verbose    = kwargs.get('verbose', True)

        remember = kwargs.get('remember_problems', False)

        for epoch in range(n_epochs):
            t = time.time()

            epoch_loss = 0
            n_batches = 0

            final_losses = []
            for inputs in iterate_minibatches(*train_inputs, batch_size=batch_size):
                loss, median_final_loss, thetas = self.train_on_batch(inputs, n_iter, **kwargs)

                final_losses.append(median_final_loss)
                train_loss.append(loss)

                epoch_loss += loss
                n_batches += 1
                
                if remember and epoch == n_epochs - 1:
                    problems += list(thetas[:, -1])

            epoch_loss /= n_batches

            if val_inputs:
                val_loss.append(self.validation_loss(val_inputs, n_iter, batch_size, **kwargs))

            if verbose:
                print("Epoch number: {epoch}".format(epoch=epoch))
                print("\tTime: {time}".format(time=time.time() - t))
                print("\tTrain loss: {train_loss}".format(train_loss=epoch_loss))
                print("\tMedian final loss: {final_loss}".format(final_loss=np.median(final_losses)))

                if val_inputs:
                    print("\tValidation loss: {val_loss}".format(val_loss=val_loss[-1]))

            self.lr.set_value((self.lr.get_value() * decay_rate).astype(np.float32))

        training_result = {
            'train_loss': train_loss
        }

        if val_inputs:
            training_result['val_loss'] = val_loss

        if remember:
            training_result['problems'] = (np.array(problems),) + train_inputs[1:]

        return training_result


    def optimize_many(self, n_iter, thetas, params, **kwargs):
        return self.loss_fn(thetas, n_iter, *params)
    

    def optimize(self, n_iter, theta, params, **kwargs):
        thetas, losses = self.optimize_many(n_iter, theta[None], [p[None] for p in params], **kwargs)
        return thetas[0], losses[0]
