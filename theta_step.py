import numpy as np

import theano
import theano.tensor as T
import lasagne as L

class ThetaStep(L.layers.MergeLayer):
    def __init__(self, theta_in, delta_in, scale_output=1.0, **kwargs):

        incomings = [theta_in, delta_in]
        super(ThetaStep, self).__init__(incomings, **kwargs)

        self.theta_in = theta_in
        self.delta_in = delta_in
        self.scale_output = scale_output 
        
    def get_output_for(self, inputs, **kwargs):
        theta, delta = inputs
        return theta + np.cast['float32'](self.scale_output) * delta

    def get_output_shape_for(self, input_shapes, **kwargs):
        return input_shapes[0]
