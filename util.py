import numpy as np
import theano
import theano.tensor as T
import lasagne as L
import pickle

import matplotlib.pyplot as plt

from theano.printing import Print as TPP

def iterate_minibatches(*arrays, **kwargs):
    batch_size = kwargs.get("batch_size", 100)
    shuffle = kwargs.get("shuffle", True)

    if shuffle: 
        indices = np.arange(len(arrays[0]))
        np.random.shuffle(indices)

    start_idx = 0
    while start_idx < len(arrays[0]):
        if shuffle: 
            excerpt = indices[start_idx: start_idx + batch_size]
        else:
            excerpt = slice(start_idx, start_idx + batch_size)

        start_idx += batch_size

        yield [arr[excerpt] for arr in arrays]

def interactive_legend(ax=None):
    if ax is None:
        ax = plt.gca()
    if ax.legend_ is None:
        ax.legend()

    return InteractiveLegend(ax.legend_)

class InteractiveLegend(object):
    def __init__(self, legend):
        self.legend = legend
        self.fig = legend.axes.figure

        self.lookup_artist, self.lookup_handle = self._build_lookups(legend)
        self._setup_connections()

        self.update()

    def _setup_connections(self):
        for artist in self.legend.texts + self.legend.legendHandles:
            artist.set_picker(10) # 10 points tolerance

        self.fig.canvas.mpl_connect('pick_event', self.on_pick)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

    def _build_lookups(self, legend):
        labels = [t.get_text() for t in legend.texts]
        handles = legend.legendHandles
        label2handle = dict(zip(labels, handles))
        handle2text = dict(zip(handles, legend.texts))

        lookup_artist = {}
        lookup_handle = {}
        for artist in legend.axes.get_children():
            if artist.get_label() in labels:
                handle = label2handle[artist.get_label()]
                lookup_handle[artist] = handle
                lookup_artist[handle] = artist
                lookup_artist[handle2text[handle]] = artist

        lookup_handle.update(zip(handles, handles))
        lookup_handle.update(zip(legend.texts, handles))

        return lookup_artist, lookup_handle

    def on_pick(self, event):
        handle = event.artist
        if handle in self.lookup_artist:
            artist = self.lookup_artist[handle]
            artist.set_visible(not artist.get_visible())
            self.update()

    def on_click(self, event):
        if event.button == 3:
            visible = False
        elif event.button == 2:
            visible = True
        else:
            return

        for artist in self.lookup_artist.values():
            artist.set_visible(visible)
        self.update()

    def update(self):
        for artist in self.lookup_artist.values():
            handle = self.lookup_handle[artist]
            if artist.get_visible():
                handle.set_visible(True)
            else:
                handle.set_visible(False)
        self.fig.canvas.draw()

    def show(self):
        plt.show()


def plot_trajectory(problem, sample_runs, ax, func=None):
    #theta_opt = (a_, a_**2)
    #print(theta_opt)
    
    min_x = min(np.concatenate([v[0].T[0] for v in sample_runs.values()]))
    max_x = max(np.concatenate([v[0].T[0] for v in sample_runs.values()]))
    min_y = min(np.concatenate([v[0].T[1] for v in sample_runs.values()]))
    max_y = max(np.concatenate([v[0].T[1] for v in sample_runs.values()]))
    
    delta_x = (max_x - min_x) / 100.
    delta_y = (max_y - min_y) / 100.

    x = np.arange(2 * min_x - (min_x + max_x) / 2, 2 * max_x - (min_x + max_x) / 2, delta_x)
    y = np.arange(2 * min_y - (min_y + max_y) / 2, 2 * max_y - (min_y + max_y) / 2, delta_y)

    if func is None:
        func = lambda X, Y: (a_ - X)**2 + b_ * (Y - X**2)**2
    
    X, Y = np.meshgrid(x, y)
    #Z = (a_ - X)**2 + b_ * (Y - X**2)**2
    Z = func(X, Y)

    levels=np.logspace(-6, 6, num=13)
    CS = ax.contour(X, Y, Z, levels=levels)
    ax.clabel(CS, inline=1, fontsize=10)

    for name in sorted(sample_runs.keys()):
        history, lss = sample_runs[name]
        ax.plot(history.T[0], history.T[1], label=name, marker='x')
    
    #ax.plot([theta_opt[0]], [theta_opt[1]], marker='o', color='k', markersize=10)            
    
    ax.set_title('Trajectory')
    ax.set_xlabel('x1')
    ax.set_ylabel('x2')
    #ax.legend();
    interactive_legend(ax)

def get_trajectory(n_iter, problem, optimizers, lr=0.001):
    theta = problem[0]
    params = problem[1:]

    sample_runs = {}

    for name, opt in optimizers.items():
        #if name != 'lbfgs':
        history, losses = opt.optimize(n_iter, theta, params, lr=lr if name != 'lbfgs' else 1.0)
        history = np.concatenate([theta.reshape(1, -1), history], axis=0)
        sample_runs[name] = (history, losses)
        #else:
        #    history, losses = fn(theta, n_iter, a_, b_, 1.0)
        #    history = np.concatenate([theta.reshape(1, -1), history], axis=0)
        #    losses = list(losses)
        #    losses += [np.min(losses)] * (n_iter - len(losses))
        #    sample_runs[name] = history, losses
    
    return sample_runs

def draw_trajectory(n_iter, problem, optimizers, func=None):
    sample_runs = get_trajectory(n_iter, problem, optimizers)
    
    fig, (ax_t, ax_loss) = plt.subplots(2, 1, figsize=(12, 12))
    
    plot_trajectory(problem, sample_runs, ax_t, func)
    
    ax_loss.set_title('loss/step')
    ax_loss.set_xlabel('step')
    ax_loss.set_ylabel('loss')

    for name in sorted(sample_runs.keys()):
        _, losses = sample_runs[name]
        ax_loss.semilogy(losses, label=name)

    ax_loss.legend();
    fig.tight_layout();

def get_moving_loss(loss):
    moving_loss = [loss[0]]
    for i in loss[1:]:
        moving_loss.append(0.9 * moving_loss[-1] + 0.1 * i)
    return moving_loss

def make_sgd(func, var=None):
    def sgd_step(theta, lr):
        f = func(theta)
        g = theano.grad(f, theta)
        return theta - lr * g, f

    input_var = T.vector()
    n_steps = T.iscalar()

    sgd_lr = T.scalar()
    sgd_thetas, sgd_losses = theano.scan(fn=sgd_step,
                                         outputs_info=[input_var, None],
                                         non_sequences=sgd_lr,
                                         n_steps=n_steps)[0]

    sgd_fn = theano.function([input_var, n_steps] + var + [sgd_lr], 
                             [sgd_thetas, sgd_losses], allow_input_downcast=True)
    return sgd_fn

def make_momentum(func, mu_=0.9, var=None):
    def momentum_step(theta, old_grad, lr, mu):
        f = func(theta)
        g = theano.grad(f, theta)
        new_grad = mu * old_grad + g
        return theta - lr * new_grad, f, new_grad

    input_var = T.vector()
    n_steps = T.iscalar()

    momentum_mu = T.scalar()
    momentum_lr = T.scalar()

    momentum_thetas, momentum_losses, _ = theano.scan(fn=momentum_step,
                                                      outputs_info=[input_var, None, T.zeros_like(input_var)],
                                                      non_sequences=[momentum_lr, momentum_mu],
                                                      n_steps=n_steps)[0]

    momentum_fn = theano.function([input_var, n_steps] + var + [momentum_lr], 
                                  [momentum_thetas, momentum_losses], 
                                  allow_input_downcast=True, 
                                  givens={ momentum_mu: np.cast['float32'](0.9), })

    return momentum_fn

def make_lbfgs(func, init_lr=0.001, m=20, var=None):

    def lbfgs_step_1(s, y, q):
        rho = 1. / y.dot(s)
        alpha = rho * s.dot(q)
        q = q - alpha * y

        return q, rho, alpha


    def lbfgs_step_2(rho, s, y, alpha, z):
        beta = rho * y.dot(z)
        z = z + s * (alpha - beta)
        return z

    bfgs_lr = T.scalar('learning_rate')

    def step_lr(lr, theta, z, cur_f, *args):
        f = func(theta - lr * z)
        return lr / 2, theano.scan_module.until(f < cur_f)

    def lbfgs_step(theta, num, old_thetas, old_grads):
        f = func(theta)
        g = theano.grad(f, theta)

        new_thetas = T.concatenate([theta.dimshuffle('x', 0), old_thetas[:-1]])
        new_grads = T.concatenate([g.dimshuffle('x', 0), old_grads[:-1]])

        s = new_thetas[:num] - old_thetas[:num]
        y = new_grads[:num] - old_grads[:num]

        q = g

        q, rho, alpha = theano.scan(fn=lbfgs_step_1,
                                    sequences=[s, y],
                                    outputs_info=[q, None, None],
                                    n_steps=num)[0]
        q = q[-1]

        z = q * s[0].dot(y[0]) / y[0].dot(y[0])

        z = theano.scan(fn=lbfgs_step_2,
                        sequences=[rho[::-1], s[::-1], y[::-1], alpha[::-1]],
                        outputs_info=z,
                        n_steps=num)[0][-1]

        lr = theano.scan(fn=step_lr,
                         outputs_info=(2 * bfgs_lr),
                         non_sequences=[theta, z, f] + var,
                         n_steps=20)[0][-1]

        num = T.min(T.concatenate([T.constant(m).dimshuffle('x'), (num + 1).dimshuffle('x')]))
        #return [theta - lr * z, num, f, new_thetas, new_grads], theano.scan_module.until(s[0].dot(s[0]) < 1e-8)
        return [theta - lr * z, num, f, new_thetas, new_grads], theano.scan_module.until((z * lr).dot(z * lr) < 1e-8)


    input_var = T.vector()
    n_steps = T.iscalar('n_steps')

    f = func(input_var)
    g = theano.grad(f, input_var)

    old_thetas = T.matrix()
    old_grads = T.matrix()
        
    thetas, _, losses, _, _ = theano.scan(fn=lbfgs_step,
                                       outputs_info=[
                                            input_var - bfgs_lr * 1e-3 * g, 
                                            T.constant(1), None, old_thetas, old_grads
                                        ],
                                       n_steps=n_steps)[0]

    lbfgs_fn = theano.function([input_var, n_steps] + var + [bfgs_lr], 
                               [thetas, losses], 
                               allow_input_downcast=True,
                               givens={
                                    old_thetas: T.concatenate([input_var.dimshuffle('x', 0), T.zeros((m-1, g.shape[0]))]),
                                    old_grads: T.concatenate([g.dimshuffle('x', 0), T.zeros((m-1, g.shape[0]))]),
                               })

    return lbfgs_fn

def save_opt(opt, filename):
    with open(filename, 'wb') as f:
        net_params = {'params': L.layers.get_all_param_values(opt.net['l_rec'])}
        pickle.dump(net_params, f, protocol=pickle.HIGHEST_PROTOCOL)
        
def load_opt(opt, filename):
    with open(filename, 'rb') as f:
        net_params = pickle.load(f)
        params = net_params['params']
        L.layers.set_all_param_values(opt.net['l_rec'], params)
