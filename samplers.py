import numpy as np

def get_partition(problems, values, levels, filter_empty=True):
    #v, p = zip(*sorted(zip(values, problems), key=lambda x: x[0]))
    v = np.argsort(values)
    p = tuple(x[v] for x in problems)
    v = values[v]

    partition = []

    levels = np.array([-np.inf] + sorted(levels) + [np.inf])

    for i in range(len(levels)-1):
        index = np.where(np.logical_and(levels[i] < v, v <= levels[i+1]))[0]

        if filter_empty and len(index) == 0:
            continue
        else:
            partition.append(tuple(x[index] for x in p))

    if filter_empty:
        return list(filter(lambda x: len(x) > 0, partition[::-1]))
    else:
        return partition[::-1]


def from_optimizer(inputs, opt, n_iter, levels, retain=8000, **kwargs):
    thetas, losses = opt.optimize_many(n_iter, inputs[0], inputs[1:], **kwargs)
    thetas = thetas[:, 1:]
    losses = losses[:, 1:]

    def new_param(p):
        new_p = np.tile(p, (n_iter,) + (1,) * p.ndim)
        new_p = new_p.transpose((1, 0) + tuple(range(2, p.ndim)))
        return new_p.reshape(-1, *p[0].shape)

    new_thetas = thetas.reshape(-1, thetas.shape[2])

    def make_new_param(p):
        new_p = np.tile(p, (n_iter,) + (1,) * p.ndim)
        new_p = new_p.transpose((1, 0) + tuple(range(2, new_p.ndim)))
        new_p = new_p.reshape(-1, *p[0].shape)
        return new_p

    #new_params = tuple(np.tile(p, (n_iter,) + (1,) * p.ndim).reshape(-1, *p[0].shape) for p in inputs[1:])
    new_params = tuple(make_new_param(p) for p in inputs[1:])

    partition = get_partition((new_thetas,) + new_params, losses.reshape(-1), levels, filter_empty=False)

    partition = [list(p) for p in partition]

    for i in range(len(partition)):
        indices = np.arange(len(partition[i][0]))
        np.random.shuffle(indices)

        for j in range(len(partition[i])):
            partition[i][j] = partition[i][j][indices][:retain]

    return partition
