import numpy as np

import theano
import theano.tensor as T
import lasagne as L

from theano.printing import Print as TPP

class ChainStep(L.layers.MergeLayer):
    def __init__(self, incoming, params, opts, function, n_small_steps, n_steps, **kwargs):
        incomings = [incoming] + params
        super(ChainStep, self).__init__(incomings, **kwargs)

        self.function = function
        self.opts = opts
        self.n_small_steps = n_small_steps
        self.n_steps = n_steps

    def get_output_shape_for(self, input_shapes):
        return input_shapes[0]

    def get_params(self, **tags):
        return sum([opt.get_params(**tags) for opt in self.opts], [])

    def get_output_for(self, inputs, **kwargs):
        theta = inputs[0]
        params = inputs[1:]

        def step(theta, n_small_steps, *params):
            thetas, losses = zip(*[opt.optimize_sym(n_small_steps, theta, params, self.function) for opt in self.opts])
            
            thetas = T.concatenate([t.dimshuffle(('x',) + tuple(range(t.ndim))) for t in thetas])
            losses = T.concatenate([l.dimshuffle(('x',) + tuple(range(l.ndim))) for l in losses])

            best_opt_index = T.argmin(losses[:, :, -1], axis=0)
            
            new_theta = thetas[best_opt_index, T.arange(thetas.shape[1]), -1]
            loss = losses[best_opt_index, T.arange(thetas.shape[1]), -1]

            return new_theta, loss, best_opt_index

        (thetas, losses, indexes), updates = theano.scan(step,
                                       outputs_info=[T.unbroadcast(theta, 0), None, None],
                                       non_sequences=[self.n_small_steps] + params,
                                       n_steps=self.n_steps)

        thetas = T.concatenate([theta.dimshuffle('x', 0, 1), thetas], axis=0)
        losses = T.concatenate([self.function(theta, *params).dimshuffle('x', 0), losses], axis=0)

        self.updates = updates

        return thetas.dimshuffle(1, 0, 2), losses.dimshuffle(1, 0), indexes.dimshuffle(1, 0)
