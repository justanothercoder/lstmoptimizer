import numpy as np
import theano
import theano.tensor as T

from collections import OrderedDict
from optimizer import Optimizer
    

class MomentumOptimizer(Optimizer):
    def __init__(self, function, params, **kwargs):
        super(MomentumOptimizer, self).__init__(**kwargs)
    
        input_var = T.matrix()
        n_steps = T.iscalar()
        lr = T.scalar()
        mu = T.scalar()

        thetas, losses = self.optimize_sym(n_steps, input_var, params, function, lr=lr, mu=mu)
        self.fn = theano.function([input_var, n_steps, lr, mu] + params, [thetas, losses], allow_input_downcast=True)
    

    def optimize(self, n_iter, theta, params, lr=0.01, mu=0.9, **kwargs):
        thetas, losses = self.optimize_many(n_iter, theta[None], [p[None] for p in params], lr=lr, mu=mu, **kwargs)
        return thetas[0], losses[0]


    def optimize_many(self, n_iter, thetas, params, lr=0.01, mu=0.9, **kwargs):
        return self.fn(thetas, n_iter, lr, mu, *params)
    
    
    def optimize_sym(self, n_iter, theta, params, function, lr=T.scalar(), mu=T.scalar(), **kwargs):
        def step(theta, old_grad, lr, mu, *args):
            f = function(theta, *args)
            g = theano.grad(f.sum(), theta).reshape(theta.shape)
            new_grad = mu * old_grad + g
            return theta - lr * new_grad, f, new_grad

        thetas, losses, _ = theano.scan(fn=step,
                                        outputs_info=[theta, None, T.zeros_like(theta)],
                                        non_sequences=[lr, mu] + params,
                                        n_steps=n_iter)[0]

        thetas = T.concatenate([theta.dimshuffle('x', 0, 1), thetas], axis=0)
        losses = T.concatenate([losses, function(thetas[-1], *params).dimshuffle('x', 0)], axis=0)

        return thetas.dimshuffle(1, 0, 2), losses.dimshuffle(1, 0)
    
    
    def optimize_sym_grad(self, theta, grad, lr=0.01, mu=0.9, **kwargs):
        value = theta.get_value(borrow=True)
        velocity = theano.shared(np.zeros(value.shape, dtype=value.dtype), broadcastable=theta.broadcastable)

        delta = mu * velocity - lr * grad

        updates = OrderedDict()
        updates[velocity] = delta

        return delta, updates
