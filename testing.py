import numpy as np
import matplotlib.pyplot as plt

from collections import namedtuple
from trainable_optimizer import TrainableOptimizer

TestCase = namedtuple('TestCase', 'n_iter n_functions')

def test_optimizers(optimizers, n_iter, n_functions, **testing_options):
    normalize = testing_options.get('normalize', True)
    sampler = testing_options['sampler']

    test_inputs = sampler(n_functions)

    thetas = test_inputs[0]
    params = test_inputs[1:]

    histories = {}
    
    lrates = np.logspace(0, 29, num=30, base=2.0) * 1e-6

    for name, opt in optimizers.items():
        print("Testing {}".format(name))

        if isinstance(opt, TrainableOptimizer):
            losses = opt.optimize_many(n_iter, thetas, params)[1]
            if normalize:
                histories[name] = np.median(losses / losses[:, 0].reshape(-1, 1), axis=0)
            else: 
                histories[name] = np.median(losses, axis=0)

        elif name == 'lbfgs':
            losses = opt.optimize_many(n_iter, thetas, params, m=10, lr=1.0)[1]
            if normalize:
                histories[name] = np.median(losses / losses[:, 0].reshape(-1, 1), axis=0)
            else:
                histories[name] = np.median(losses, axis=0)
            
        else:
            best_lrate = None
            best_loss = None
            best_history = None
        
            for lrate in lrates:
                losses = opt.optimize_many(n_iter, thetas, params, lr=lrate)[1]
                if np.isnan(losses).any():
                    break
                    
                if normalize:
                    loss = np.median(losses / losses[:, 0].reshape(-1, 1), axis=0)[-1]
                else:
                    loss = np.median(losses, axis=0)[-1]

                if best_loss is None or best_loss > loss:
                    best_loss = loss
                    best_lrate = lrate
                    best_history = np.median(losses, axis=0)
                    
            histories["{}; lr={}".format(name, best_lrate)] = best_history
            
    return histories

def run_tests(tests, optimizers, **testing_options):
    tests_results = []

    for n_iter, n_functions in tests:
        histories = test_optimizers(optimizers, n_iter=n_iter, n_functions=n_functions, **testing_options)
        tests_results.append(histories)

    return tests_results

def plot_tests_results(tests, tests_results):
    n_tests = len(tests)
    fig, axes = plt.subplots(n_tests, figsize=(12, 6 * n_tests))

    if n_tests == 1:
        axes = (axes,)

    for ax, (n_iter, n_functions), h in zip(axes, tests, tests_results):
        for name, hist in h.items():
            ax.semilogy(hist, label=name)
            ax.set_title('functions: {}; iterations: {}'.format(n_functions, n_iter))
        ax.legend()
