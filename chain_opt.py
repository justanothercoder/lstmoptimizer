import numpy as np

import theano
import theano.tensor as T
import lasagne as L

import agentnet
from agentnet import Recurrence
from agentnet.memory import LSTMCell
from collections import OrderedDict

from func_layer import FuncLayer
from grad_layer import GradLayer
from preprocess_layer import PreprocessLayer
from opt_step import OptStep
from theta_step import ThetaStep
from index_layer import IndexLayer
from lstm_step import LSTMStep

Inp = L.layers.InputLayer
Reshape = L.layers.ReshapeLayer

from chain_step import ChainStep
        
def make_chain_net(opts, function, params, **kwargs):

    input_var = T.matrix('input_var_inside')
    n_steps = T.iscalar('n_steps_inside')

    n_small_steps = T.iscalar('n_small_steps_inside')

    l_input = Inp(shape=(None, None), input_var=input_var)

    l_params = [Inp(shape=(None,) * param.ndim, input_var=param) for param in params]
    l_chain = ChainStep(l_input, l_params, opts, function, n_small_steps, n_steps)

    return {
        'l_chain': l_chain,

        'theta': input_var,
        'n_steps': n_steps,
        'n_small_steps': n_small_steps,

        'params': l_params,
    }
