import numpy as np

import theano
import theano.tensor as T
import lasagne as L

class HessLayer(L.layers.MergeLayer):
    def __init__(self, incoming, function, l_params=None, **kwargs):

        l_params = l_params or []
        incomings = [incoming] + l_params

        super(HessLayer, self).__init__(incomings, **kwargs)

        self.function = function

    def get_output_for(self, inputs, **kwargs):
        function = self.function(*inputs)
        N, D = inputs[0].shape

        hess = theano.gradient.disconnected_grad(theano.gradient.hessian(function.sum(), inputs[0]).reshape((N, D, D)))
        return hess

    def get_output_shape_for(self, input_shapes):
        N, D = input_shapes[0]
        return (N, D, D)
