import numpy as np

import theano
import theano.tensor as T
import lasagne as L

class NTMPreprocessLayer(L.layers.MergeLayer):
    def __init__(self, incoming, read_in, mem_in,
                 preprocess_input=True, p=10., **kwargs):
        incomings = [incoming, read_in, mem_in]
        super(NTMPreprocessLayer, self).__init__(incomings, **kwargs)

        self.preprocess_input = preprocess_input
        self.p = p

    def get_output_shape_for(self, input_shapes):
        num_out = 1 + int(self.preprocess_input) + 1
        return (input_shapes[0][0], num_out)

    def get_output_for(self, inputs, **kwargs):
        input, read, mem = inputs
        input = input.dimshuffle(0, 'x')

        if self.preprocess_input:
            lognorm = T.switch(T.abs_(input) > T.exp(-self.p), T.log(T.abs_(input)) / self.p, T.ones_like(input) * (-1))
            sign    = T.switch(T.abs_(input) > T.exp(-self.p), T.sgn(input), T.exp(self.p) * input)
            input = T.concatenate([lognorm, sign], axis=-1)
        
        def elem_dot(A, B):
            return (A[..., None] * B[..., None, :]).sum(axis=-2)
        
        #input_vector = mem.dot(read)
        input_vector = elem_dot(mem, read.dimshuffle(0, 1, 'x')).reshape(input.shape)
        input = T.concatenate([input, input_vector], axis=-1)

        return input
