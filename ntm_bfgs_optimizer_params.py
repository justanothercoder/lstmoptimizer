import time
import numpy as np

import theano
import theano.tensor as T
import lasagne as L

import agentnet
from agentnet import Recurrence
from agentnet.memory import LSTMCell
from collections import OrderedDict

from func_layer import FuncLayer
from grad_layer import GradLayer
from hess_layer import HessLayer

from ntm_preprocess_layer import NTMPreprocessLayer
from ntm_opt_step import NTMOptStep
from mem_update_layer import MemUpdateLayer
from theta_step import ThetaStep

from lstm_step import LSTMStep
from index_layer import IndexLayer
from mult_layer import MultLayer

from trainable_optimizer import TrainableOptimizer

Inp = L.layers.InputLayer
Slice = L.layers.SliceLayer
Reshape = L.layers.ReshapeLayer
        
class NTM_BFGS_Optimizer(TrainableOptimizer):
    def __init__(self, num_units, 
                 function, 
                 n_layers=2, n_gac=0, 
                 memdot=False, plusgrad=False, use_hess=False, learn_grad_coef=False,
                 use_function_values=False, scale_output=1.0,
                 preprocess_input=True, p=10., loglr=True,
                 gradient_steps=-1, grad_clipping=0,
                 params_input=None, input_var=None, n_steps=None,
                 **kwargs):

        super(NTM_BFGS_Optimizer, self).__init__()

        self.num_units = num_units

        input_var = input_var or T.matrix()
        n_steps = n_steps or T.iscalar()

        params_input = params_input or []

        l_input   = Inp(shape=(None, None), input_var=input_var, name='theta_inner')
        l_read_in = Inp(shape=(None, None)                     , name='read_inner')
        l_mem_in  = Inp(shape=(None, None, None)               , name='mem_inner')
        
        l_params       = [Inp(shape=(None,), input_var=param_input) for param_input in params_input]
        l_params_outer = [Inp(shape=(None,), input_var=param_input) for param_input in params_input]

        l_func = FuncLayer(l_input, function, l_params, name='func_layer')
        l_grad = GradLayer(l_input, function, l_params, name='grad_layer')
        l_hess = HessLayer(l_input, function, l_params, name='hess_layer')

        n_func, n_coords = input_var.shape

        l_lstm = NTMPreprocessLayer(Reshape(l_grad, (n_func * n_coords,)), 
                                    l_read_in, l_mem_in, 
                                    preprocess_input=preprocess_input, p=p, name='ntm_prep')

        #l_lstm = Reshape(l_lstm, (n_func * n_coords, l_lstm.output_shape[2]), name='reshape_prep')

        recurrent_connections = OrderedDict({ })

        for i in range(n_layers):
            l_lstm_cell = Inp(shape=(None, num_units), name='cell_{}_in'.format(i))
            l_lstm_hid  = Inp(shape=(None, num_units), name='hid_{}_in'.format(i))
            l_lstm = LSTMStep(l_lstm, l_lstm_cell, l_lstm_hid, num_units=num_units, n_gac=n_gac, grad_clipping=grad_clipping, name='lstm_{}'.format(i))

            l_cell = IndexLayer(l_lstm, 0, name='index_cell_{}'.format(i))
            l_hid  = IndexLayer(l_lstm, 1, name='index_hid_{}'.format(i))

            l_lstm = l_hid

            recurrent_connections.update({l_cell: l_lstm_cell, l_hid: l_lstm_hid})
        
        #l_lstm = L.layers.ReshapeLayer(l_lstm, (n_func, n_coords, l_lstm.output_shape[1]))

        #l_lstm = L.layers.DenseLayer(l_lstm, num_units=(1 + int(loglr) + 3 + int(learn_grad_coef)), b=None, name='dense')
        l_lstm = MultLayer(l_lstm, num_input=num_units, num_out=(1 + int(loglr) + 3 + int(learn_grad_coef)))

        l_lstm = Reshape(l_lstm, (n_func, n_coords, -1), name='reshape_out')

        l_a    = Reshape(Slice(l_lstm, indices=1, axis=2), (n_func, n_coords), name='a')
        l_b    = Reshape(Slice(l_lstm, indices=2, axis=2), (n_func, n_coords), name='b')
        l_read = Reshape(Slice(l_lstm, indices=3, axis=2), (n_func, n_coords), name='read')
        
        l_mem = MemUpdateLayer(l_mem_in, l_a, l_b, hess_in=(l_hess if use_hess else None), name='mem_update')

        l_opt = NTMOptStep(l_lstm, l_input.output_shape,
                           l_mem_in, loglr, memdot, 
                           grad_in=(l_grad if plusgrad else None), 
                           learn_grad_coef=learn_grad_coef,
                           name='ntm_opt_step')

        l_opt = ThetaStep(l_input, l_opt, input_var, scale_output=scale_output, name='theta_step')

        recurrent_connections.update({
            l_read: l_read_in,
            l_mem: l_mem_in,
            l_opt: l_input
        })

        batch_size = input_var.shape[0] * input_var.shape[1]

        #mem_init = T.zeros((n_func, input_var.shape[1], input_var.shape[1]))
        #if memdot:
        mem_init = T.tile(T.eye(input_var.shape[1]), (n_func, 1, 1))
        
        l_rec = Recurrence(
            state_variables=recurrent_connections,
            n_steps=n_steps,
            tracked_outputs=[l_opt, l_func],
            batch_size=batch_size,
            none_outs=(l_func,),
            gradient_steps=gradient_steps,
            unroll_scan=False,
            state_init={
                l_opt: Inp(shape=(None, None), input_var=input_var),
                #l_mem: Inp(shape=(None, None, None), input_var=T.tile(T.eye(batch_size), (n_func, 1, 1)))
                l_mem: Inp(shape=(None, None, None), input_var=mem_init),
                l_read: Inp(shape=(None, None), input_var=T.zeros_like(input_var))
            },
            input_nonsequences={l_param: l_param_outer for l_param, l_param_outer in zip(l_params, l_params_outer)}
        )
        
        self.l_opt = l_opt
        self.l_rec = l_rec

        self.input_var = input_var
        self.params_input = params_input
        self.n_steps = n_steps
        
        self.sym_value = L.layers.get_output(l_func)

    def get_params(self, **kwargs):
        return L.layers.get_all_params(self.l_rec, **kwargs)

    def get_params_values(self):
        return L.layers.get_all_param_values(self.l_rec)

    def get_output(self, inputs=None, **kwargs):
        if inputs is not None:
            inputs = {self.l_input: inputs}

        _, outputs = self.l_rec.get_sequence_layers() 
        updates = self.l_rec.updates

        #return L.layers.get_output(self.l_rec, inputs=inputs, **kwargs)

        outputs = L.layers.get_output(outputs, inputs=inputs, **kwargs)
        return outputs, updates

    def get_net(self):
        return self.l_rec
    
    #def get_updates(self, loss, params):
    #    theta = T.concatenate([x.flatten() for x in params])
    #    #n_coords = theta.shape[0]
    #    n_coords = np.sum([np.prod(x.get_value().shape) for x in params])
    #    
    #    cells = []
    #    hids = []

    #    for _ in range(self.n_layers):
    #        cell_init = theano.shared(np.zeros((n_coords, self.num_units), dtype=np.float32))
    #        hid_init  = theano.shared(np.zeros((n_coords, self.num_units), dtype=np.float32))

    #        cells.append(cell_init)
    #        hids.append(hid_init)

    #    updates = OrderedDict()

    #    grads = theano.grad(loss, params)
    #    input_n = T.concatenate([x.flatten() for x in grads]).dimshuffle(0, 'x')
    #    if self.preprocess_input:
    #        lognorm = T.switch(T.abs_(input_n) > T.exp(-self.p), T.log(T.abs_(input_n)) / self.p, T.ones_like(input_n) * (-1))
    #        sign = T.switch(T.abs_(input_n) > T.exp(-self.p), T.sgn(input_n), T.exp(self.p) * input_n)

    #        input_n = T.concatenate([lognorm, sign], axis=1)

    #    if self.use_function_values:
    #        input_n = T.concatenate([input_n, T.ones_like(input_n) * func], axis=1)

    #    for step, cell_previous, hid_previous in zip(self.steps, cells, hids):
    #        cell, hid = step(input_n, cell_previous, hid_previous)
    #        input_n = hid

    #        updates[cell_previous] = cell
    #        updates[hid_previous] = hid

    #    dtheta = hid.dot(self.W_hidden_to_output).dimshuffle(0)
    #    new_theta = theta + dtheta * self.scale_output

    #    cur_pos = 0
    #    for p in params:
    #        next_pos = cur_pos + np.prod(p.get_value().shape)
    #        updates[p] = T.reshape(new_theta[cur_pos:next_pos], p.shape)
    #        cur_pos = next_pos

    #    return updates
