import time
import numpy as np

import theano
import theano.tensor as T
import lasagne as L

from recurrence import Recurrence
from index_layer import IndexLayer

from trainable_optimizer import TrainableOptimizer
from chain_opt import make_chain_net

from util import iterate_minibatches

       
class ChainOptimizer(TrainableOptimizer):
    def __init__(self, function, params, opts, loss_type='sum', lambd=1e-5, compile_train=False, **kwargs):
        super(ChainOptimizer, self).__init__(**kwargs)
        self.opts = opts
        self.net = make_chain_net(opts, function, params)
        
        input_var = T.matrix()
        n_steps = T.iscalar('n_steps_outside')
        n_small_steps = T.iscalar('n_small_steps_outside')
        
        thetas, losses, indexes = self.optimize_sym(n_steps, input_var, params, function, n_small_steps=n_small_steps, **kwargs)
        self.params_init = L.layers.get_all_param_values(self.net['l_chain'])

        self.loss_type = loss_type

        loss = self.optimizer_loss(losses, loss_type, M=T)
        loss += lambd * L.regularization.regularize_network_params(self.net['l_chain'], L.regularization.l2)
                
        self.lr = theano.shared(np.array(0.01, dtype=np.float32))

        lstm_params = L.layers.get_all_params(self.net['l_chain'], trainable=True)
        updates = L.updates.adam(loss, lstm_params, learning_rate=self.lr)
        updates.update(self.updates)
        
        t = time.time()
        self.loss_fn = theano.function([input_var, n_steps, n_small_steps] + params, [thetas, losses, indexes], allow_input_downcast=True, updates=self.updates)
        print("Time compiling loss_fn: {}".format(time.time() - t))

        if compile_train:
            t = time.time()
            self.train_fn = theano.function([input_var, n_steps, n_small_steps] + params, [thetas, losses, indexes], updates=updates, allow_input_downcast=True)
            print("Time compiling train_fn: {}".format(time.time() - t))

    
    def train_on_partition(self, partition, val_inputs=None, **kwargs):
     
        losses = []

        for j, opt in enumerate(self.opts):
            if kwargs.get('verbose'):
                print("Training optimizer number {}".format(j))
                t = time.time()

            training_result = opt.train(partition[j], val_inputs, **kwargs)
            train_loss = training_result['train_loss']
            val_loss = training_result['val_loss']

            losses.append((train_loss, val_loss))

        return losses


    def optimize_many(self, n_iter, theta, params, n_small_steps=20, **kwargs):
        thetas, losses, indexes = self.loss_fn(theta, n_iter, n_small_steps, *params)
        self.indexes = indexes
        return thetas, losses


    def optimize_sym(self, n_iter, thetas, params, function, n_small_steps=T.iscalar(), **kwargs):
        l_chain = self.net['l_chain']
        
        inputs = dict(zip(self.net['params'], params))

        outputs = L.layers.get_output(l_chain, inputs=inputs)
        self.updates = l_chain.updates

        outputs = theano.clone(outputs, replace={
            self.net['n_small_steps']: n_small_steps,
            self.net['n_steps']: n_iter,
            self.net['theta']: thetas,
        })

        return outputs

    
    def train_on_batch(self, batch, n_iter, n_small_steps=20, **kwargs):
        theta = batch[0]
        params = batch[1:]

        thetas, losses = self.train_fn(theta, n_iter, n_small_steps, *params)
        median_final_loss = np.median(losses, axis=0)[-1]
        train_loss = self.optimizer_loss(losses, self.loss_type) / theta.shape[0]

        return train_loss, median_final_loss, thetas


    def validation_loss(self, val_inputs, n_iter, batch_size, n_small_steps=20, **kwargs):
        val_loss = 0
        n_batches = 0

        for inputs in iterate_minibatches(*val_inputs, batch_size=batch_size):
            _, loss_history = self.loss_fn(inputs[0], n_iter, n_small_steps, *inputs[1:])

            val_loss += self.optimizer_loss(loss_history, self.loss_type) / batch_size
            n_batches += 1
            
            print("Validation: batch number {}".format(n_batches))

        val_loss /= n_batches
        return val_loss
