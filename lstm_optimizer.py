import time
import numpy as np

import theano
import theano.tensor as T
import lasagne as L

import agentnet
from agentnet import Recurrence
from agentnet.memory import LSTMCell
from collections import OrderedDict

from grad_layer import GradLayer
from opt_step import OptStep
from theta_step import ThetaStep
from preprocess_layer import PreprocessLayer
from index_layer import IndexLayer
from lstm_step import LSTMStep

from trainable_optimizer import TrainableOptimizer

class LSTM_Optimizer(TrainableOptimizer):
    def __init__(self, num_units,
                 function,
                 n_layers=2, n_gac=0,  
                 use_function_values=False, scale_output=1.0,
                 preprocess_input=True, p=10., loglr=True,
                 gradient_steps=-1, grad_clipping=0,
                 params_input=None, input_var=None, n_steps=None, 
                 **kwargs):
        
        super(LSTM_Optimizer, self).__init__()

        self.num_units = num_units
        input_var = input_var or T.vector()
        n_steps = n_steps or T.iscalar()

        l_input = L.layers.InputLayer(shape=(None,), input_var=input_var)
        l_grad = GradLayer(l_input, function)
        l_func = IndexLayer(l_grad, 1)

        l_lstm = IndexLayer(l_grad, 0)
        l_lstm = PreprocessLayer(l_lstm, preprocess_input=preprocess_input, p=p, use_function_values=use_function_values)

        recurrent_connections = OrderedDict({ })

        for i in range(n_layers):
            l_lstm_cell = L.layers.InputLayer(shape=(None, num_units))
            l_lstm_hid = L.layers.InputLayer(shape=(None, num_units))
            l_lstm = LSTMStep(l_lstm, l_lstm_cell, l_lstm_hid, num_units=num_units, n_gac=n_gac, grad_clipping=grad_clipping, name='lstm_{}'.format(i))

            l_cell = IndexLayer(l_lstm, 0)
            l_hid  = IndexLayer(l_lstm, 1)

            l_lstm = l_hid

            recurrent_connections.update({l_cell: l_lstm_cell, l_hid: l_lstm_hid})

        l_opt = OptStep(l_lstm, num_units, loglr=loglr)
        l_opt = ThetaStep(l_input, l_opt, input_var, scale_output=scale_output)
        recurrent_connections[l_opt] = l_input

        l_rec = Recurrence(
            state_variables=recurrent_connections,
            n_steps=n_steps,
            tracked_outputs=[l_opt, l_func],
            batch_size=input_var.shape[0],
            none_outs=(l_func,),
            gradient_steps=gradient_steps,
            unroll_scan=False,
            state_init={l_opt: L.layers.InputLayer(shape=(None,), input_var=input_var)}
        )

        self.l_opt = l_opt
        self.l_rec = l_rec

        self.input_var = input_var
        self.n_steps = n_steps

        self.sym_value = L.layers.get_output(l_func)


    def get_params(self, **kwargs):
        return L.layers.get_all_params(self.l_rec, **kwargs)


    def get_params_values(self):
        return L.layers.get_all_param_values(self.l_rec)


    def get_output(self, inputs=None, **kwargs):
        if inputs is not None:
            inputs = {self.l_input: inputs}

        _, outputs = self.l_rec.get_sequence_layers() 
        updates = self.l_rec.updates

        #return L.layers.get_output(self.l_rec, inputs=inputs, **kwargs)
        outputs = L.layers.get_output(outputs, inputs=inputs, **kwargs)
        outputs = [var.T for var in outputs]
        return outputs, updates

    def get_net(self):
        return self.l_rec
        
    #def get_updates(self, loss, params):
    #    theta = T.concatenate([x.flatten() for x in params])
    #    n_coords = np.sum([np.prod(x.get_value().shape) for x in params])
    #    
    #    cells = []
    #    hids = []

    #    for _ in range(self.n_layers):
    #        cell_init = theano.shared(np.zeros((n_coords, self.num_units), dtype=np.float32))
    #        hid_init  = theano.shared(np.zeros((n_coords, self.num_units), dtype=np.float32))

    #        cells.append(cell_init)
    #        hids.append(hid_init)

    #    updates = OrderedDict()

    #    grads = theano.grad(loss, params)
    #    input_n = T.concatenate([x.flatten() for x in grads]).dimshuffle(0, 'x')
    #    if self.preprocess_input:
    #        lognorm = T.switch(T.abs_(input_n) > T.exp(-self.p), T.log(T.abs_(input_n)) / self.p, T.ones_like(input_n) * (-1))
    #        sign = T.switch(T.abs_(input_n) > T.exp(-self.p), T.sgn(input_n), T.exp(self.p) * input_n)

    #        input_n = T.concatenate([lognorm, sign], axis=1)

    #    if self.use_function_values:
    #        input_n = T.concatenate([input_n, T.ones_like(input_n) * func], axis=1)

    #    for step, cell_previous, hid_previous in zip(self.steps, cells, hids):
    #        cell, hid = step(input_n, cell_previous, hid_previous)
    #        input_n = hid

    #        updates[cell_previous] = cell
    #        updates[hid_previous] = hid

    #    dtheta = hid.dot(self.W_hidden_to_output).dimshuffle(0)
    #    new_theta = theta + dtheta * self.scale_output

    #    cur_pos = 0
    #    for p in params:
    #        next_pos = cur_pos + np.prod(p.get_value().shape)
    #        updates[p] = T.reshape(new_theta[cur_pos:next_pos], p.shape)
    #        cur_pos = next_pos

    #    return updates

