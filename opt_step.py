import numpy as np

import theano
import theano.tensor as T
import lasagne as L

from theano.printing import Print as TPP

class OptStep(L.layers.Layer):
    def __init__(self, incoming, num_units, loglr=True,
                 W_hidden_to_output=L.init.GlorotUniform(),
                 **kwargs):

        super(OptStep, self).__init__(incoming, **kwargs)

        self.loglr = loglr
        self.num_out = 1 + int(loglr)
        self.W_hidden_to_output = self.add_param(W_hidden_to_output, (num_units, self.num_out), name='W_hidden_to_output', regularizable=False)

    def get_output_shape_for(self, input_shape):
        return (input_shape[0],)

    def get_output_for(self, input, **kwargs):
        hid = input
        
        out = hid.dot(self.W_hidden_to_output)
        if self.loglr:
            dtheta = T.exp(out[:, 1]) * out[:, 0]
        else:
            dtheta = out[:, 0]

        return dtheta
