import time
import numpy as np

import theano
import theano.tensor as T
import lasagne as L

from trainable_optimizer import TrainableOptimizer
from lstm_opt import make_lstm_net

Inp = L.layers.InputLayer
Reshape = L.layers.ReshapeLayer

class LSTM_Optimizer(TrainableOptimizer):
    def __init__(self, function, params, loss_type='sum', lambd=1e-5, compile_fn=True, **kwargs):
        super(LSTM_Optimizer, self).__init__()

        self.function = function
        self.params = params
        self.loss_type = loss_type

        self.net = make_lstm_net(function, params, **kwargs)

        input_var = T.matrix()
        n_steps = T.iscalar('n_steps_outside')

        thetas, losses = self.optimize_sym(n_steps, input_var, params, function, **kwargs)
        self.params_init = L.layers.get_all_param_values(self.net['l_rec'])

        loss = self.optimizer_loss(losses, loss_type, M=T)
        loss += lambd * L.regularization.regularize_network_params(self.net['l_rec'], L.regularization.l2)
                
        self.lr = theano.shared(np.array(0.01, dtype=np.float32))

        lstm_params = L.layers.get_all_params(self.net['l_rec'], trainable=True)
        updates = L.updates.adam(loss, lstm_params, learning_rate=self.lr)
        updates.update(self.updates)

        if compile_fn:
            t = time.time()
            self.loss_fn = theano.function([input_var, n_steps] + params, [thetas, losses], allow_input_downcast=True, updates=self.updates)
            print("Time compiling loss_fn: {}".format(time.time() - t))

            t = time.time()
            self.train_fn = theano.function([input_var, n_steps] + params, [thetas, losses], updates=updates, allow_input_downcast=True)
            print("Time compiling train_fn: {}".format(time.time() - t))

    
#    def copy_opt(self, **kwargs):
#        opt = LSTM_Optimizer(self.function, self.params, compile_fn=False, **kwargs)
#        
#        old_params = self.get_params()
#        new_params = opt.get_params()
#
#        t = time.time()
#        opt.train_fn = self.train_fn.copy(swap={old_param: new_param for old_param, new_param in zip(old_params, new_params)})
#        print("Time copying train_fn: {}".format(time.time() - t))
#
#        t = time.time()
#        opt.loss_fn  = self.loss_fn.copy(swap={old_param: new_param for old_param, new_param in zip(old_params, new_params)})
#        print("Time copying loss_fn: {}".format(time.time() - t))
#
#        return opt

        
    def optimize_sym(self, n_iter, thetas, params, function, **kwargs):
        self.updates = self.net['l_rec'].updates

        inputs = dict(zip(self.net['params'], params))
        inputs.update(dict(zip(self.net['params_'], params)))

        thetas_hist = self.net['thetas']
        losses_hist = self.net['losses']

        outputs = L.layers.get_output([thetas_hist, losses_hist], inputs=inputs)
        outputs = theano.clone(outputs, replace={
            self.net['n_steps']: n_iter,
            self.net['theta']: thetas,
        })

        return outputs
    

    def get_params(self, **kwargs):
        return L.layers.get_all_params(self.net['l_rec'], **kwargs)
    

    def optimize_sym_grad(self, theta, grad, lr=0.01, **kwargs):
        #return -lr * grad, OrderedDict()
        raise NotImplementedError


    def reset_network(self):
        L.layers.set_all_param_values(self.net['l_rec'], self.params_init)
