import numpy as np

import theano
import theano.tensor as T
import lasagne as L

from theano.printing import Print as TPP

class NTMOptStep(L.layers.MergeLayer):
    def __init__(self, incoming, out_shape, memory_in=None, loglr=True, memdot=False, 
                 grad_in=None, learn_grad_coef=False, **kwargs):

        incomings = [incoming]

        if memdot:
            self.memory_index = len(incomings)
            incomings += [memory_in]

        if grad_in:
            self.grad_index = len(incomings)
            incomings += [grad_in]

        super(NTMOptStep, self).__init__(incomings, **kwargs)

        self.grad_in = grad_in
        self.memdot = memdot
        self.loglr = loglr
        self.learn_grad_coef = learn_grad_coef
        self.num_out = 1 + int(loglr) + 3 + int(learn_grad_coef) # a, b, r + delta + loglr

        self.out_shape = out_shape

    def get_output_shape_for(self, input_shapes):
        #return (self.input_shapes[1][0], self.input_shapes[1][1])
        return self.out_shape
        
    def get_output_for(self, inputs, **kwargs):
        input = inputs[0]

        if self.memdot:
            memory = inputs[self.memory_index]

        if self.grad_in:
            grad = inputs[self.grad_index]

        dtheta = input[:, :, 0]
        
        if self.loglr:
            dtheta = T.exp(input[:, :, 4]) * dtheta

        if self.learn_grad_coef:
            grad_coef = input[:, :, 4 + int(self.loglr)]
            grad = grad * grad_coef

        #def elem_dot(A, B):
        #    return (A[..., None] * B[..., None, :]).sum(axis=-2)
        def elem_dot(A, B):
            return (A[..., None] * B[..., None, :, :]).sum(axis=-2)

        if self.memdot:
            #dtheta = memory.dot(dtheta)
            dtheta = elem_dot(memory, dtheta.dimshuffle(0, 1, 'x')).dimshuffle(0, 1)

        if self.grad_in:
            dtheta = dtheta - grad

        return dtheta
