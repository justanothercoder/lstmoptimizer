import numpy as np

import theano
import theano.tensor as T
import lasagne as L

#def elem_dot(A, B):
#    return (A[..., None] * B[..., None, :]).sum(axis=-2)

def elem_dot(A, B):
    return (A[..., None] * B[..., None, :, :]).sum(axis=-2)

class MemUpdateLayer(L.layers.MergeLayer):
    def __init__(self, mem_in, a_in, b_in, hess_in=None, **kwargs):

        incomings = [mem_in, a_in, b_in]
            
        self.hess_in = hess_in
        if hess_in:
            incomings.append(hess_in)

        super(MemUpdateLayer, self).__init__(incomings, **kwargs)

    def get_output_for(self, inputs, **kwargs):
        if self.hess_in:
            _, _, _, hess = inputs
            return T.nlinalg.pinv(hess)
        else:
            mem, a, b = inputs
            a = a.dimshuffle(0, 1, 'x')
            b = b.dimshuffle(0, 'x', 1)
            return mem + elem_dot(a, b)

    def get_output_shape_for(self, input_shapes):
        return input_shapes[0]
