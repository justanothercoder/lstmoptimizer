import numpy as np
import theano
import theano.tensor as T

from collections import OrderedDict
from optimizer import Optimizer

from theano.printing import Print as TPP
        
def lbfgs_step_1(s, y, q):
    rho = (y * s).sum(axis=1)
    alpha = (s * q).sum(axis=1) / rho
    q = q - alpha.dimshuffle(0, 'x') * y

    return q, rho, alpha


def lbfgs_step_2(rho, s, y, alpha, z):
    beta = (y * z).sum(axis=1) / rho
    z = z + s * (alpha - beta).dimshuffle(0, 'x')

    return z

        
class LBFGS_Optimizer(Optimizer):
    def __init__(self, function, params, **kwargs):
        super(LBFGS_Optimizer, self).__init__(**kwargs)
            
        input_var = T.matrix()
        n_steps = T.iscalar('n_steps')
        lr = T.scalar('learning_rate')
        m = T.iscalar('m')

        thetas, losses = self.optimize_sym(n_steps, input_var, params, function, lr=lr, m=m)
        self.fn = theano.function([input_var, n_steps, lr, m] + params, [thetas, losses], allow_input_downcast=True)
   
    
    def optimize(self, n_iter, theta, params, lr=0.01, m=20, **kwargs):
        thetas, losses = self.optimize_many(n_iter, theta[None], [p[None] for p in params], lr=lr, m=m, **kwargs)
        return thetas[0], losses[0]
   
   
    def optimize_many(self, n_iter, thetas, params, lr=1.0, m=20, **kwargs):
        thetas, losses = self.fn(thetas, n_iter, lr, m, *params)
        index = np.isnan(losses).argmax(axis=1)

        for i, ind in enumerate(index):
            if ind > 0:
                thetas[i][ind:] = thetas[i][ind - 1]
                losses[i][ind:] = losses[i][ind - 1]

        return thetas, losses
    
    
    def optimize_sym(self, n_iter, thetas, params, function, lr=T.scalar(), m=T.iscalar(), **kwargs):
        def step_lr(lr, theta, z, cur_f, *args):
            f = function(theta - lr.dimshuffle(0, 'x') * z, *args)

            condition = (f < cur_f)
            return T.switch(condition, lr, lr / 2.), theano.scan_module.until(T.all(condition))

        def lbfgs_step(theta, num, old_thetas, old_grads, bfgs_lr, *args):
            f = function(theta, *args)
            g = theano.grad(f.sum(), theta).reshape(theta.shape)

            new_thetas = T.concatenate([theta.dimshuffle('x', 0, 1), old_thetas[:-1]], axis=0)
            new_grads = T.concatenate([g.dimshuffle('x', 0, 1), old_grads[:-1]], axis=0)

            s = new_thetas[:num] - old_thetas[:num]
            y = new_grads[:num] - old_grads[:num]

            q = g

            q, rho, alpha = theano.scan(fn=lbfgs_step_1,
                                        sequences=[s, y],
                                        outputs_info=[q, None, None],
                                        n_steps=num)[0]
            q = q[-1]

            z = q * (s[0] * y[0]).sum(axis=1, keepdims=True) / (y[0] * y[0]).sum(axis=1, keepdims=True)

            z = theano.scan(fn=lbfgs_step_2,
                            sequences=[rho[::-1], s[::-1], y[::-1], alpha[::-1]],
                            outputs_info=z,
                            n_steps=num)[0][-1]

            init_lr = T.ones((theta.shape[0],)) * (bfgs_lr)

            lr = theano.scan(fn=step_lr,
                             outputs_info=init_lr,
                             non_sequences=[theta, z, f] + list(args),
                             n_steps=20)[0][-1].dimshuffle(0, 'x')

            new_num = T.minimum(m, num + 1)

            #condition = (((z * lr)**2).sum(axis=1, keepdims=True) < 1e-8)
            condition = T.sqrt((g**2).sum(axis=1, keepdims=True)) < 1e-8
        
            return [T.switch(condition, theta, theta - lr * z), 
                    new_num, 
                    f, 
                    T.switch(condition, old_thetas, new_thetas), 
                    T.switch(condition, old_grads, new_grads)], theano.scan_module.until(T.all(condition))

        f = function(thetas, *params)
        g = theano.grad(f.sum(), thetas).reshape(thetas.shape)

        old_thetas = T.concatenate([thetas.dimshuffle('x', 0, 1), T.zeros((m-1, g.shape[0], g.shape[1]))], axis=0)
        old_grads = T.concatenate([g.dimshuffle('x', 0, 1), T.zeros((m-1, g.shape[0], g.shape[1]))], axis=0)

        lbfgs_thetas, _, lbfgs_losses, _, _ = theano.scan(fn=lbfgs_step,
                                                          outputs_info=[
                                                            thetas - lr * 1e-3 * g,
                                                            T.constant(1).astype('int32'), None, old_thetas, old_grads
                                                          ],
                                                          non_sequences=[lr] + params,
                                                          n_steps=n_iter)[0]

        lbfgs_thetas = T.concatenate([thetas.dimshuffle('x', 0, 1), (thetas - lr * 1e-3 * g).dimshuffle('x', 0, 1), lbfgs_thetas[:-1]], axis=0)
        lbfgs_losses = T.concatenate([f.dimshuffle('x', 0), lbfgs_losses], axis=0)

        return lbfgs_thetas.dimshuffle(1, 0, 2), lbfgs_losses.dimshuffle(1, 0)
    
    
    def optimize_sym_grad(self, theta, grad, lr=1.0, m=20, **kwargs):
        value = theta.get_value(borrow=True)
        
        old_thetas = theano.shared(np.zeros((m, values.shape[0]), dtype=value.dtype, broadcastable=theta.broadcastable))
        old_grads = theano.shared(np.zeros((m, values.shape[0]), dtype=value.dtype, broadcastable=theta.broadcastable))
        num = theano.shared(np.cast['float32'](1.))

        new_thetas = T.concatenate([theta.dimshuffle('x', 0), old_thetas[:-1]])
        new_grads = T.concatenate([grad.dimshuffle('x', 0), old_grads[:-1]])

        s = new_thetas[:num] - old_thetas[:num]
        y = new_grads[:num] - old_grads[:num]

        q = g

        q, rho, alpha = theano.scan(fn=lbfgs_step_1,
                                    sequences=[s, y],
                                    outputs_info=[q, None, None],
                                    n_steps=num)[0]
        q = q[-1]

        z = q * s[0].dot(y[0]) / y[0].dot(y[0])

        z = theano.scan(fn=lbfgs_step_2,
                        sequences=[rho[::-1], s[::-1], y[::-1], alpha[::-1]],
                        outputs_info=z,
                        n_steps=num)[0][-1]

        def step_lr(lr, theta, z, cur_f, *args):
            f = function(theta - lr * z, *args)
            return lr / 2, theano.scan_module.until(f < cur_f)

        lr = theano.scan(fn=step_lr,
                         outputs_info=(2 * lr),
                         non_sequences=[theta, z, f] + params,
                         n_steps=20)[0][-1]

        new_num = T.min(T.concatenate([T.constant(m).dimshuffle('x'), (num + 1).dimshuffle('x')]))
        delta = -lr * z

        updates = OrderedDict()
        updates[old_thetas] = new_thetas
        updates[old_grads] = new_grads
        updates[num] = new_num

        return delta, updates
