import numpy as np
import theano
import theano.tensor as T

from collections import OrderedDict
from optimizer import Optimizer
    
class SgdOptimizer(Optimizer):
    def __init__(self, function, params, **kwargs):
        super(SgdOptimizer, self).__init__(**kwargs)
        
        input_var = T.matrix()
        n_steps = T.iscalar()
        lr = T.scalar()

        thetas, losses = self.optimize_sym(n_steps, input_var, params, function, lr=lr)
        self.fn = theano.function([input_var, n_steps, lr] + params, [thetas, losses], allow_input_downcast=True)


    def optimize(self, n_iter, theta, params, lr=0.01, **kwargs):
        thetas, losses = self.optimize_many(n_iter, theta[None], [p[None] for p in params], lr=lr, **kwargs)
        return thetas[0], losses[0]


    def optimize_many(self, n_iter, thetas, params, lr=0.01, **kwargs):
        return self.fn(thetas, n_iter, lr, *params)

    
    def optimize_sym(self, n_iter, theta, params, function, lr=T.scalar(), **kwargs):
        def step(theta, lr, *args):
            f = function(theta, *args)
            g = theano.grad(f.sum(), theta).reshape(theta.shape)
            return theta - lr * g, f

        thetas, losses = theano.scan(fn=step,
                                     outputs_info=[theta, None],
                                     non_sequences=[lr] + params,
                                     n_steps=n_iter)[0]
        
        thetas = T.concatenate([theta.dimshuffle('x', 0, 1), thetas], axis=0)
        losses = T.concatenate([losses, function(thetas[-1], *params).dimshuffle('x', 0)], axis=0)

        return thetas.dimshuffle(1, 0, 2), losses.dimshuffle(1, 0)
    
    
    def optimize_sym_grad(self, theta, grad, lr=0.01, **kwargs):
        return -lr * grad, OrderedDict()
