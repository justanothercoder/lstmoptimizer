import numpy as np

import theano
import theano.tensor as T
import lasagne as L

class MultLayer(L.layers.Layer):
    def __init__(self, incoming, num_input, num_out, W=L.init.GlorotUniform(), **kwargs):

        super(MultLayer, self).__init__(incoming, **kwargs)

        self.num_out = num_out
        self.W = self.add_param(W, (num_input, num_out), regularizable=False, name='W')

    def get_output_for(self, input, **kwargs):
        return input.dot(self.W)

    def get_output_shape_for(self, input_shape, **kwargs):
        return (input_shape[0], self.num_out)
