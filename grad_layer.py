import numpy as np

import theano
import theano.tensor as T
import lasagne as L

class GradLayer(L.layers.MergeLayer):
    def __init__(self, incoming, function, l_params=None, **kwargs):

        l_params = l_params or []
        incomings = [incoming] + l_params

        super(GradLayer, self).__init__(incomings, **kwargs)

        self.function = function

    def get_output_for(self, inputs, **kwargs):
        function = self.function(*inputs)
        #grad = theano.gradient.disconnected_grad(theano.grad(function, inputs[0]))
        grad = theano.gradient.disconnected_grad(theano.grad(function.sum(), inputs[0]).reshape(inputs[0].shape))

        #hess = theano.gradient.disconnected_grad(theano.gradient.hessian(function, input))
        #return grad, function, hess
        return grad

    def get_output_shape_for(self, input_shapes):
        return input_shapes[0]
